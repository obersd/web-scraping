drop table if exists tracks;
drop table if exists albums;
drop table if exists artists;
drop table if exists subgenres;
drop table if exists genres;

create table genres (
    genreId integer not null primary key autoincrement,
    name text not null,
    url text not null
);

create table subgenres (
    subgenreId integer not null primary key autoincrement,
    genreId integer no null constraint fk_subgenres_genreId references genres(genreId),
    name text not null,
    url text not null
);

create table artists (
    artistId text not null primary key,
    name text not null
);

create table albums (
    albumId text not null primary key,
    name text
);

create table tracks (
    trackId text not null primary key,
    albumId text not null constraint fk_tracks_albumid references albums(albumId),
    artistId text null constraint fk_tracks_artistid references artists(artistId),
    subgenreId integer null constraint fk_tracks_subgenreid references subgenres(subgenreId),
    name text not null,
    url text not null
);