// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace Api
{
    using System.Reflection;
    using A2net.Swagger.SwaggerMiddleware;
    using A2net.Swagger.SwaggerService;
    using AppServices;
    using AppServices.DataAccess;
    using AppServices.Impl;
    using Mappers;
    using Microsoft.EntityFrameworkCore;
    using NapsterExtractor;
    using NapsterExtractor.Impl;

    public class Startup
    {
        internal const string CorsAllowOrigins = "CorsAllowOrigins";

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<INapsterScraping, NapsterScraping>(
                _ => new NapsterScraping(NapsterScraping.Napster));
            services.AddAutoMapper(expression =>
            {
                expression.AddProfile<MapperProfile>();
                expression.AddProfile<ApiMapperProfile>();
            });
            services.AddScoped<IExtractor, Extractor>();
            services.AddScoped<IMusicDataImporter, MusicDataImporter>();
            services.AddScoped<IMusicFinder, MusicFinder>();
            services.AddDbContext<WebScrapingContext>(builder =>
                {
                    builder.UseSqlite(this.Configuration.GetConnectionString("Db"));
                    builder.EnableSensitiveDataLogging();
                }
            );
            
            services.AddCors(options =>
            {
                options.AddPolicy(CorsAllowOrigins,
                    builder => { builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); });
            });
            
            services.AddSwaggerService(new OpenApiOptions
            {
                Name = "v1",
                ApiDescription = "Documentation API",
                ApiTitle = "Web Scraping API",
                ApiVersion = "Alpha",
                ContactEmail = "avargass0200@egresado.ipn.mx",
                ContactName = "Hello world!",
                ContactUrl = "https://lexvargas.com",
                XmlDocuumentationPaths = new[] {$"{Assembly.GetExecutingAssembly().GetName().Name}.xml"},
                OperationFilters = new List<KeyValuePair<object[], Type>>
                {
                    new(Array.Empty<object>(), typeof(ContentTypeFilter))
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerAndUi("Web Scraping API V1");
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors(CorsAllowOrigins);
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}