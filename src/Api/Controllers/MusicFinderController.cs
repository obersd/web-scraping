﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AppServices;
    using AutoMapper;
    using Dtos;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// Buscador de música.
    /// </summary>
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [EnableCors(Startup.CorsAllowOrigins)]
    [Route("api/music")]
    public class MusicFinderController : ControllerBase
    {
        private readonly IMusicFinder musicFinder;
        private readonly IMapper mapper;
        private readonly IMusicDataImporter importer;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="musicFinder">Buscador.</param>
        /// <param name="mapper">Mapeo.</param>
        /// <param name="importer">Importador.</param>
        public MusicFinderController(IMusicFinder musicFinder, IMapper mapper,IMusicDataImporter importer)
        {
            this.musicFinder = musicFinder;
            this.mapper = mapper;
            this.importer = importer;
        }
        
        /// <summary>
        /// Incializa la base de datos. Se desaconseja el uso de este método en favor de la ejecución de la prueba
        /// unitaria para generar los registros. 
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Create()
        {
            await this.importer.CreateSourcesAsync();
            return this.Ok();
        }

        /// <summary>
        /// Géneros por palabra clave.
        /// </summary>
        /// <param name="genre">palabra clave del género, por ejemplo: POP, PO...</param>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("genre")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<GenreResponse>))]
        public async Task<IActionResult> Genres([FromQuery(Name = "q")] string genre = "")
        {
            var genres = await this.musicFinder.GetGenresAsync(genre);
            return this.Ok(this.mapper.Map<IList<GenreResponse>>(genres));
        }
        
        /// <summary>
        /// Album por artista.
        /// </summary>
        /// <param name="artist">Id de artista o palabra clave del artista.</param>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("album/artist")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<AlbumArtistResponse>))]
        public async Task<IActionResult> AlbumsByArtist([FromQuery(Name = "q")] string artist = "")
        {
            var albums = await this.musicFinder.GetAlbumsByArtistAsync(artist);
            return this.Ok(this.mapper.Map<IList<AlbumArtistResponse>>(albums));
        }
        
        /// <summary>
        /// Artista por canción.
        /// </summary>
        /// <param name="track">Id. de canción o palabra clave de canción.</param>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("artist/track")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ArtistTrackResponse>))]
        public async Task<IActionResult> ArtistsByTrack([FromQuery(Name = "q")] string track = "")
        {
            var artists = await this.musicFinder.GetArtistByTrackAsync(track);
            return this.Ok(this.mapper.Map<IList<ArtistTrackResponse>>(artists));
        }
        
        /// <summary>
        /// Artista por género.
        /// </summary>
        /// <param name="gender">Palabra clave de género.</param>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("artist/genre")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ArtistGenreResponse>))]
        public async Task<IActionResult> ArtistsByGender([FromQuery(Name = "q")] string gender = "")
        {
            var artists = await this.musicFinder.GetArtistsByGenderAsync(gender);
            return this.Ok(this.mapper.Map<IList<ArtistGenreResponse>>(artists));
        }
        
        /// <summary>
        /// Canción por album.
        /// </summary>
        /// <param name="album">Id de album o palabra clave de canción.</param>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("track/album")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TrackResponse>))]
        public async Task<IActionResult> TrackByAlbum([FromQuery(Name = "q")] string album = "")
        {
            var tracks = await this.musicFinder.GetTracksByAlbumAsync(album);
            return this.Ok(this.mapper.Map<IList<TrackResponse>>(tracks));
        }
    }
}