// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MappingProfile.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Api.Mappers
{
    using AppServices.DataModel;
    using AutoMapper;
    using Dtos;

    public class ApiMapperProfile : Profile
    {
        public ApiMapperProfile()
        {
            this.CreateMap<Genre, GenreResponse>()
                .ForMember(dest => dest.Genre, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Subgenres, opt => opt.MapFrom(src => src.SubgenresString))
                .ForAllOtherMembers(x => x.Ignore());
            
            this.CreateMap<AlbumWithArtist,AlbumArtistResponse>()
                .ForMember(dest=>dest.Album,opt=>opt.MapFrom(src=>src.Album))
                .ForMember(dest=>dest.Artist,opt=>opt.MapFrom(src=>src.Artist))
                .ForMember(dest=>dest.AlbumId,opt=>opt.MapFrom(src=>src.AlbumId))
                .ForMember(dest=>dest.ArtistId,opt=>opt.MapFrom(src=>src.ArtistId))
                .ForAllOtherMembers(x => x.Ignore());
            
            this.CreateMap<ArtistWithTrack,ArtistTrackResponse>()
                .ForMember(dest=>dest.Track,opt=>opt.MapFrom(src=>src.Track))
                .ForMember(dest=>dest.Artist,opt=>opt.MapFrom(src=>src.Artist))
                .ForMember(dest=>dest.TrackId,opt=>opt.MapFrom(src=>src.TrackId))
                .ForMember(dest=>dest.ArtistId,opt=>opt.MapFrom(src=>src.ArtistId))
                .ForAllOtherMembers(x => x.Ignore());
            
            this.CreateMap<ArtistWithGenre,ArtistGenreResponse>()
                .ForMember(dest=>dest.Genre,opt=>opt.MapFrom(src=>src.Genre))
                .ForMember(dest=>dest.Artist,opt=>opt.MapFrom(src=>src.Artist))
                .ForMember(dest=>dest.ArtistId,opt=>opt.MapFrom(src=>src.ArtistId))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<Track, TrackResponse>()
                .ForMember(dest => dest.Track, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.TrackId, opt => opt.MapFrom(src => src.TrackId))
                .ForMember(dest => dest.AlbumArtist, opt => opt.MapFrom((track, _) => new AlbumArtistResponse()
                {
                    Album = track.Album?.Name,
                    Artist = track.Artist?.Name,
                    AlbumId = track.AlbumId,
                    ArtistId = track.ArtistId
                }))
                .ForAllOtherMembers(x => x.Ignore());

        }
    }
}