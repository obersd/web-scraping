// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using Fizzler.Systems.HtmlAgilityPack;
    using HtmlAgilityPack;
    using Model;

    public class NapsterScrapingTest : NapsterScraping, INapsterScraping
    {
        public NapsterScrapingTest(string uri) : base(uri)
        {
        }

        public new (TimeSpan ElapsedTime, IList<WebGenre> Genres) GetData()
        {
            var timer = new Stopwatch();
            timer.Start();
            var genres = GetGenresEx(this.uri);
            Task.WaitAll(genres.Select(item => Task.Factory.StartNew(() => this.GetData(item, new HtmlWeb())))
                .ToArray<Task>());
            timer.Stop();
            return (timer.Elapsed, genres);
        }

        private static IList<WebGenre> GetGenresEx(string url)
        {
            var web = new HtmlWeb();
            var html = web.Load($"{url}/music");
            var list = html.DocumentNode.QuerySelectorAll(".genre-list").FirstOrDefault();
            if (list == null)
            {
                return Array.Empty<WebGenre>();
            }

            return (from genre in new List<HtmlNode> {list.QuerySelectorAll(".genre-item").First()}
                select genre.ChildNodes.FirstOrDefault(x => x.Name == "a")
                into alink
                let p = alink?.ChildNodes.FirstOrDefault(x => x.Name == "p")
                where alink != null && p != null
                select new WebGenre(p.ChildNodes.FirstOrDefault(x => x.Name == "span")?.InnerHtml,
                    alink.GetAttributeValue("href", string.Empty))).ToList();
        }
    }
}