// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NapsterScraping.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using Fizzler.Systems.HtmlAgilityPack;
    using HtmlAgilityPack;
    using Model;

    public class NapsterScraping : INapsterScraping
    {
        protected readonly string uri;
        public const string Napster = "https://us.napster.com";

        public NapsterScraping(string uri) => this.uri = uri;

        public (TimeSpan ElapsedTime, IList<WebGenre> Genres) GetData()
        {
            var timer = new Stopwatch();
            timer.Start();
            var genres = GetGenres(this.uri);
            Task.WaitAll(genres.Select(item => Task.Factory.StartNew(() => this.GetData(item, new HtmlWeb())))
                .ToArray<Task>());
            timer.Stop();
            return (timer.Elapsed, genres);
        }

        internal WebGenre GetData(WebGenre webGenres, HtmlWeb web)
        {
            GetSubgenres(this.uri, webGenres, web);
            foreach (var subgenreItem in webGenres.Subgenres)
            {
                GetTrackInfo(this.uri, subgenreItem, web);
                foreach (var albumItem in subgenreItem.Albums)
                {
                    GetAlbum(this.uri, albumItem, web);
                }
            }

            return webGenres;
        }

        private static void GetAlbum(string url, WebAlbum webAlbum, HtmlWeb web)
        {
            var html = web.Load($"{url}/{webAlbum.Tracks[0].Url}");
            foreach (var trackItem in html.GetElementbyId("album-tracks")?.QuerySelectorAll(".track-item") ??
                                      Array.Empty<HtmlNode>())
            {
                var attrs = trackItem.GetAttributes()?.ToList() ?? Array.Empty<HtmlAttribute>().ToList();
                var trackId = attrs.FirstOrDefault(x => x.Name == "track_id")?.Value;
                var href = attrs.FirstOrDefault(x => x.Name == "href")?.Value;
                var trackname = attrs.FirstOrDefault(x => x.Name == "track_name")?.Value;
                webAlbum.AddTrack(trackId, trackname, href);
            }
        }

        private static void GetTrackInfo(string url, WebSubGenre webSubGenre, HtmlWeb web)
        {
            var html = web.Load($"{url}/{webSubGenre.Url}");
            foreach (var trackItem in html.GetElementbyId("top-tracks").SelectNodes("ul[1]").Nodes()
                .Where(x => x.Name == "li"))
            {
                var attrs = trackItem.GetAttributes()?.ToList() ?? Array.Empty<HtmlAttribute>().ToList();
                var artistId = attrs.FirstOrDefault(x => x.Name == "artist_id")?.Value;
                var artist = webSubGenre.GetArtist(artistId);
                if (artist == null)
                {
                    var artistName = attrs.FirstOrDefault(x => x.Name == "artist_name")?.Value;
                    artist = webSubGenre.TryAddArtist(artistId, artistName);
                }

                var albumId = attrs.FirstOrDefault(x => x.Name == "album_id")?.Value;
                var album = webSubGenre.GetAlbum(albumId);
                if (album == null)
                {
                    var albumName = attrs.FirstOrDefault(x => x.Name == "album_name")?.Value;
                    album = webSubGenre.TryAddAlbum(albumId, albumName, artist);
                }

                album.AddTrack(attrs.FirstOrDefault(x => x.Name == "track_id")?.Value,
                    attrs.FirstOrDefault(x => x.Name == "track_name")?.Value,
                    attrs.FirstOrDefault(x => x.Name == "href")?.Value);
            }
        }

        private static void GetSubgenres(string url, WebGenre webGenre, HtmlWeb web)
        {
            var html = web.Load($"{url}/{webGenre.Url}");
            foreach (var subgenre in html.DocumentNode.QuerySelectorAll(".genre-item").ToList()
                .Select(subgenreItem => subgenreItem.SelectNodes("span[1]/a[1]").FirstOrDefault())
                .Where(subgenre => subgenre != null))
            {
                webGenre.AddSubgene(new WebSubGenre(subgenre.InnerHtml,
                    subgenre.GetAttributeValue("href", string.Empty)));
            }
        }

        private static IList<WebGenre> GetGenres(string url)
        {
            var web = new HtmlWeb();
            var html = web.Load($"{url}/music");
            var list = html.DocumentNode.QuerySelectorAll(".genre-list").FirstOrDefault();
            if (list == null)
            {
                return Array.Empty<WebGenre>();
            }

            return (from genre in list.QuerySelectorAll(".genre-item")
                select genre.ChildNodes.FirstOrDefault(x => x.Name == "a")
                into alink
                let p = alink?.ChildNodes.FirstOrDefault(x => x.Name == "p")
                where alink != null && p != null
                select new WebGenre(p.ChildNodes.FirstOrDefault(x => x.Name == "span")?.InnerHtml,
                    alink.GetAttributeValue("href", string.Empty))).ToList();
        }

        /// <inheritdoc />
        public void Dispose() => GC.SuppressFinalize(this);

        /// <inheritdoc />
        // ReSharper disable once EmptyDestructor
        ~NapsterScraping()
        {
        }
    }
}