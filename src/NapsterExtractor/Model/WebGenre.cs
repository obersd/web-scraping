// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Model
{
    using System;
    using System.Collections.Generic;
    using A2net.Base.Extensions;

    public class WebGenre
    {
        public WebGenre(string name, string url)
        {
            this.Name = name.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase);
            this.Url = url;
            this.Subgenres = new List<WebSubGenre>();
        }

        public void AddSubgene(WebSubGenre item) => this.Subgenres.Add(item);
        public IList<WebSubGenre> Subgenres { get; init; }
        public string Name { get; init; }
        public string Url { get; init; }

        public override string ToString()
        {
            return
                $"{nameof(this.Name)}: {this.Name}, {nameof(this.Url)}: {this.Url}, {nameof(this.Subgenres)}: {this.Subgenres.Count}";
        }
    }
}