// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using A2net.Base.Extensions;

    public class WebSubGenre
    {
        public string Name { get; init; }
        public string Url { get; init; }

        public IList<WebAlbum> Albums { get; init; }
        public IList<WebArtist> Artists { get; init; }

        public override string ToString()
        {
            return
                $"{nameof(this.Name)}: {this.Name}, {nameof(this.Url)}: {this.Url}, {nameof(this.Albums)}: {this.Albums.Count}, {nameof(this.Artists)}: {this.Artists.Count}";
        }

        public WebArtist TryAddArtist(string id, string name)
        {
            var artist = this.GetArtist(id.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase));
            if (artist != null)
            {
                return artist;
            }

            artist = new WebArtist(id, name);
            this.Artists.Add(artist);
            return artist;
        }

        public WebAlbum TryAddAlbum(string id, string name, WebArtist webArtist)
        {
            var album = this.GetAlbum(id.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase));
            if (album != null)
            {
                return album;
            }

            album = new WebAlbum(id, name, webArtist);
            this.Albums.Add(album);
            return album;
        }

        public WebAlbum GetAlbum(string id) =>
            this.Albums.FirstOrDefault(x => string.Equals(x.Id,
                id.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase),
                StringComparison.OrdinalIgnoreCase));

        public WebArtist GetArtist(string id) =>
            this.Artists.FirstOrDefault(x => string.Equals(x.Id,
                id.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase),
                StringComparison.OrdinalIgnoreCase));

        public WebSubGenre(string name, string url)
        {
            this.Name = name?.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase) ?? string.Empty;
            this.Url = url;
            this.Albums = new List<WebAlbum>();
            this.Artists = new List<WebArtist>();
        }
    }
}