// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using A2net.Base.Extensions;

    public class WebAlbum
    {
        public WebAlbum(string id, string name, WebArtist webArtist)
        {
            this.Id = id;
            this.Name = name.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase);
            this.Tracks = new List<WebTrack>();
            this.WebArtist = webArtist;
        }

        public override string ToString()
        {
            return
                $"{nameof(this.Id)}: {this.Id}, {nameof(this.Name)}: {this.Name}, {nameof(this.Tracks)}: {this.Tracks.Count}";
        }

        public WebTrack AddTrack(string trackId, string trackName, string url)
        {
            var track = this.Tracks.FirstOrDefault(x =>
                string.Equals(x.TrackId, trackId, StringComparison.OrdinalIgnoreCase));
            if (track == null)
            {
                track = new WebTrack(trackId, trackName, this.WebArtist, this, url);
                this.Tracks.Add(track);
            }
            else
            {
                track.Url = url;
                track.WebAlbum = this;
                track.WebArtist = this.WebArtist;
            }

            return track;
        }

        public WebArtist WebArtist { get; set; }
        public string Id { get; init; }
        public string Name { get; init; }

        public IList<WebTrack> Tracks { get; }
    }
}