// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Model
{
    using System;
    using A2net.Base.Extensions;

    public class WebTrack
    {
        public WebTrack(string trackId, string name, WebArtist webArtist, WebAlbum webAlbum, string url)
        {
            this.Name = name?.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase) ?? string.Empty;
            this.WebArtist = webArtist;
            this.WebAlbum = webAlbum;
            this.Url = url;
            this.TrackId = trackId;
        }

        public override string ToString()
        {
            return
                $"{nameof(this.TrackId)}: {this.TrackId}, {nameof(this.Name)}: {this.Name}, {nameof(this.Url)}: {this.Url}";
        }

        public string TrackId { get; init;}
        public string Name { get; init;}
        public WebArtist WebArtist { get; set; }
        public WebAlbum WebAlbum { get; set; }
        public string Url { get; set; }
    }
}