// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor.Model
{
    using System;
    using A2net.Base.Extensions;

    public class WebArtist
    {
        public WebArtist(string id, string name)
        {
            this.Id = id;
            this.Name = name.ReplaceEx("\n", string.Empty, StringComparison.OrdinalIgnoreCase);
        }

        public override string ToString()
        {
            return $"{nameof(this.Id)}: {this.Id}, {nameof(this.Name)}: {this.Name}";
        }

        public string Id { get; init; }
        public string Name { get; init; }
    }
}