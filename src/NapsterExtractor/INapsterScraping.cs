// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NapsterExtractor
{
    using System;
    using System.Collections.Generic;
    using Model;

    public interface INapsterScraping : IDisposable
    {
        (TimeSpan ElapsedTime, IList<WebGenre> Genres) GetData();
    }
}