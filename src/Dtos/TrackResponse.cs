// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Dtos
{
    public sealed class TrackResponse
    {
        public string TrackId { get; set; }
        public string Track { get; set; }
        public AlbumArtistResponse AlbumArtist { get; set; }
    }
}