// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IExtractor.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices
{
    using System.Collections.Generic;
    using NapsterExtractor;
    using NapsterExtractor.Model;

    public interface IExtractor
    {
        IList<WebGenre> GetGenres();
    }
}