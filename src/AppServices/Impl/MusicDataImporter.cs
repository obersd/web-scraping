// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MusicDataImporter.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Impl
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataAccess;
    using DataModel;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class MusicDataImporter : IMusicDataImporter
    {
        private readonly IExtractor extractor;
        private readonly IMapper mapper;
        private readonly WebScrapingContext context;
        private readonly ILogger<MusicDataImporter> logger;

        public MusicDataImporter(IExtractor extractor, IMapper mapper, WebScrapingContext context,
            ILogger<MusicDataImporter> logger)
        {
            this.extractor = extractor;
            this.mapper = mapper;
            this.context = context;
            this.logger = logger;
        }

        public async Task CreateSourcesAsync()
        {
            var napsterGenres = this.extractor.GetGenres();
            var genres = this.mapper.Map<IList<Genre>>(napsterGenres);
            await this.context.Genres.AddRangeAsync(genres);
            await this.context.SaveChangesAsync();
            foreach (var genreItem in napsterGenres)
            {
                foreach (var subgenreItem in genreItem.Subgenres)
                {
                    foreach (var albumItem in subgenreItem.Albums)
                    {
                        foreach (var trackItem in albumItem.Tracks)
                        {
                            if (await this.context.Tracks.FindAsync(trackItem.TrackId) != null)
                            {
                                this.logger.LogWarning("*** Track encontrado: {TrackItem}", trackItem);
                                continue;
                            }

                            var track = this.mapper.Map<Track>(trackItem, options =>
                                options.AfterMap((_, track) =>
                                {
                                    var genre = genres.FirstOrDefault(x => x.Name == genreItem.Name);
                                    var subgenre = genre?.Subgenres.FirstOrDefault(x => x.Name == subgenreItem.Name);
                                    if (subgenre != null)
                                    {
                                        track.Subgenre = subgenre;
                                        track.SubgenreId = subgenre.SubgenreId;
                                    }
                                }));

                            var album = await this.context.Albums.FirstOrDefaultAsync(x => x.AlbumId == track.AlbumId);
                            if (album == null)
                            {
                                album = new Album()
                                {
                                    Name = track.Album.Name,
                                    AlbumId = track.Album.AlbumId
                                };
                                await this.context.Albums.AddAsync(album);
                                await this.context.SaveChangesAsync();
                                this.logger.LogWarning("*** Se ha añadido nuevo album: {Album}", album);
                            }

                            track.Album = album;
                            track.AlbumId = album.AlbumId;
                            var artist =
                                await this.context.Artists.FirstOrDefaultAsync(x => x.ArtistId == track.ArtistId);
                            if (artist == null)
                            {
                                artist = new Artist()
                                {
                                    Name = track.Artist.Name,
                                    ArtistId = track.Artist.ArtistId
                                };
                                await this.context.Artists.AddAsync(artist);
                                await this.context.SaveChangesAsync();
                                this.logger.LogWarning("*** Se ha añadido nuevo artista: {Artista}", artist);
                            }

                            track.Artist = artist;
                            track.ArtistId = artist.ArtistId;
                            await this.context.Tracks.AddAsync(track);
                        }
                    }
                }
            }

            await this.context.SaveChangesAsync();
        }
    }
}