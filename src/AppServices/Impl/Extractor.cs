// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extractor.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Impl
{
    using System.Collections.Generic;
    using Microsoft.Extensions.Logging;
    using NapsterExtractor;
    using NapsterExtractor.Model;

    public sealed class Extractor : IExtractor
    {
        private readonly INapsterScraping scraping;
        private readonly ILogger<Extractor> logger;

        public Extractor(INapsterScraping scraping, ILogger<Extractor> logger)
        {
            this.scraping = scraping;
            this.logger = logger;
        }

        public IList<WebGenre> GetGenres()
        {
            var (elapsedTime, genres) = this.scraping.GetData();
            this.logger.LogInformation("Elapsed seconds: {ElapsedTime}", elapsedTime.TotalSeconds);
            return genres;
        }
    }
}