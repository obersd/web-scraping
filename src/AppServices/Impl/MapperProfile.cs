// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Impl
{
    using AutoMapper;
    using DataModel;
    using NapsterExtractor;
    using NapsterExtractor.Model;

    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            this.CreateMap<WebSubGenre, Subgenre>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.Url))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<WebGenre, DataModel.Genre>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.Url))
                .ForMember(dest => dest.Subgenres, opt => opt.MapFrom(src => src.Subgenres))
                .ForAllOtherMembers(x => x.Ignore());
            
            this.CreateMap<WebArtist, DataModel.Artist>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ArtistId, opt => opt.MapFrom(src => src.Id))
                .ForAllOtherMembers(x => x.Ignore());
            
            this.CreateMap<WebAlbum, DataModel.Album>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.AlbumId, opt => opt.MapFrom(src => src.Id))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<WebTrack, DataModel.Track>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.Url))
                .ForMember(dest => dest.TrackId, opt => opt.MapFrom(src => src.TrackId))
                .ForMember(dest => dest.Album, opt => opt.MapFrom(src => src.WebAlbum))
                .ForMember(dest => dest.Artist, opt => opt.MapFrom(src => src.WebArtist))
                .ForMember(dest => dest.AlbumId, opt => opt.MapFrom((track, track1) => track.WebAlbum.Id))
                .ForMember(dest => dest.ArtistId, opt => opt.MapFrom((track, track1) => track.WebArtist.Id))
                .ForAllOtherMembers(x => x.Ignore());
        }
    }
}