// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Impl
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataAccess;
    using DataModel;
    using Microsoft.EntityFrameworkCore;

    public sealed class MusicFinder : IMusicFinder
    {
        private readonly WebScrapingContext ctx;

        public MusicFinder(WebScrapingContext ctx) => this.ctx = ctx;

        async Task<IList<Genre>> IMusicFinder.GetGenresAsync(string genre) => await this.ctx.Genres
            .Include(x => x.Subgenres).Where(x => EF.Functions.Like(x.Name, $"{genre}%")).OrderBy(x => x.Name)
            .ToListAsync();

        public async Task<IList<ArtistWithGenre>> GetArtistsByGenderAsync(string genre)
        {
            var genreEx = $"{genre}%";
            return await this.ctx.ArtistWithGenre.FromSqlInterpolated(
                @$"select distinct a.artistId, a.name as artist, g.name as genre 
                    from artists a
                    inner join tracks t on a.artistId = t.artistId
                    inner join subgenres s on t.subgenreId = s.subgenreId
                    inner join genres g on s.genreId = g.genreId
                    where g.name like {genreEx} order by 2, 3, 1;").ToListAsync();
        }

        public async Task<IList<AlbumWithArtist>> GetAlbumsByArtistAsync(string artistId)
        {
            var artistIdEx = $"{artistId}%";
            return await this.ctx.AlbumWithArtist.FromSqlInterpolated(
                    @$"select distinct a.albumId, a.name as album, a2.artistId, a2.name as artist 
                    from albums a 
                    inner join tracks t on a.albumId = t.albumId
                    inner join artists a2 on t.artistId = a2.artistId
                    where a2.name like {artistIdEx} or a2.artistId like {artistIdEx} order by 2, 4, 1, 3")
                .ToListAsync();
        }

        public async Task<IList<Track>> GetTracksByAlbumAsync(string albumId) =>
            (await this.ctx.Albums.Include(x => x.Tracks).ThenInclude(y => y.Artist).Where(a =>
                    EF.Functions.Like(a.Name, $"{albumId}%")
                    || EF.Functions.Like(a.AlbumId, $"{albumId}%"))
                .ToListAsync()).SelectMany(x => x.Tracks).OrderBy(x => x.Album.Name).ThenBy(x => x.Name)
            .ToList();

        public async Task<IList<ArtistWithTrack>> GetArtistByTrackAsync(string trackId)
        {
            var trackIdEx = $"{trackId}%";
            return await this.ctx.ArtistWithTrack.FromSqlInterpolated(
                @$"select t.trackId, t.name as track, a.artistId, a.name as artist 
                    from tracks t
                    inner join artists a on t.artistId = a.artistId
                    where  t.name like {trackIdEx} order by 2, 4, 1, 3").ToListAsync();
        }
    }
}