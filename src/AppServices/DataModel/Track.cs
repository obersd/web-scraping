﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AppServices.DataModel
{
    public partial class Track
    {
        public string TrackId { get; set; }
        public string AlbumId { get; set; }
        public string ArtistId { get; set; }
        public long? SubgenreId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public virtual Album Album { get; set; }
        public virtual Artist Artist { get; set; }
        public virtual Subgenre Subgenre { get; set; }
    }
}
