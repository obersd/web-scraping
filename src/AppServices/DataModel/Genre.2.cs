﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#nullable disable

namespace AppServices.DataModel
{
    using System.Linq;

    public partial class Genre
    {
        public string SubgenresString =>
            this.Subgenres == null ? string.Empty : string.Join(", ", this.Subgenres.Select(x => x.Name));
    }
}