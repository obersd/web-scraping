// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.DataModel
{
    public sealed class ArtistWithTrack
    {
        public string ArtistId { get; set; }
        public string Artist { get; set; }
        public string Track { get; set; }
        public string TrackId { get; set; }
    }
}