﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AppServices.DataModel
{
    public partial class Genre
    {
        public Genre()
        {
            Subgenres = new HashSet<Subgenre>();
        }

        public long GenreId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public virtual ICollection<Subgenre> Subgenres { get; set; }
    }
}
