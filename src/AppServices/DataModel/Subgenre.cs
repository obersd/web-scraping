﻿using System.Collections.Generic;

#nullable disable

namespace AppServices.DataModel
{
    public partial class Subgenre
    {
        public Subgenre()
        {
            Tracks = new HashSet<Track>();
        }

        public long SubgenreId { get; set; }
        public long? GenreId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public virtual Genre Genre { get; set; }
        public virtual ICollection<Track> Tracks { get; set; }
    }
}
