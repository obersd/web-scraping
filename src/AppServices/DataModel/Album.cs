﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AppServices.DataModel
{
    public partial class Album
    {
        public Album()
        {
            Tracks = new HashSet<Track>();
        }

        public string AlbumId { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{nameof(this.AlbumId)}: {this.AlbumId}, {nameof(this.Name)}: {this.Name}";
        }

        public virtual ICollection<Track> Tracks { get; set; }
    }
}
