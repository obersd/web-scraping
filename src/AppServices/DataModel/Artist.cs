﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AppServices.DataModel
{
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Artist
    {
        public Artist()
        {
            Tracks = new HashSet<Track>();
        }

        public override string ToString()
        {
            return $"{nameof(this.ArtistId)}: {this.ArtistId}, {nameof(this.Name)}: {this.Name}";
        }

        public string ArtistId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Track> Tracks { get; set; }
    }
}