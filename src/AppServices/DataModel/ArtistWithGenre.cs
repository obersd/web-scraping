// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArtistWithGenre.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.DataModel
{
    public sealed class ArtistWithGenre 
    {
        public string ArtistId { get; set; }
        public string Artist { get; set; }
        public string Genre { get; set; }
    }
}