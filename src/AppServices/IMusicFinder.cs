// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataModel;

    public interface IMusicFinder
    {
        Task<IList<Genre>> GetGenresAsync(string genre);
        
        Task<IList<ArtistWithGenre>> GetArtistsByGenderAsync(string genre);

        Task<IList<AlbumWithArtist>> GetAlbumsByArtistAsync(string artistId);

        Task<IList<Track>> GetTracksByAlbumAsync(string albumId);
        
        Task<IList<ArtistWithTrack>> GetArtistByTrackAsync(string trackId);
    }
}