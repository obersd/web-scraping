﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using AppServices.DataModel;

#nullable disable

namespace AppServices.DataAccess
{
    public partial class WebScrapingContext : DbContext
    {
        public WebScrapingContext()
        {
        }

        public WebScrapingContext(DbContextOptions<WebScrapingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Album> Albums { get; set; }
        public virtual DbSet<Artist> Artists { get; set; }
        public virtual DbSet<Genre> Genres { get; set; }
        public virtual DbSet<Subgenre> Subgenres { get; set; }
        public  virtual DbSet<ArtistWithGenre> ArtistWithGenre { get; set; }
        public virtual  DbSet<AlbumWithArtist> AlbumWithArtist { get; set; }
        public virtual DbSet<ArtistWithTrack> ArtistWithTrack { get; set; }
        public virtual DbSet<Track> Tracks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlite("Data Source=/home/lex/src/web-scraping/db/db.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Album>(entity =>
            {
                entity.ToTable("albums");

                entity.Property(e => e.AlbumId)
                    .HasColumnType("text")
                    .HasColumnName("albumId");

                entity.Property(e => e.Name)
                    .HasColumnType("text")
                    .HasColumnName("name");
            });

            modelBuilder.Entity<ArtistWithGenre>().HasNoKey();
            modelBuilder.Entity<AlbumWithArtist>().HasNoKey();
            modelBuilder.Entity<ArtistWithTrack>().HasNoKey();
            
            modelBuilder.Entity<Artist>(entity =>
            {
                entity.ToTable("artists");

                entity.Property(e => e.ArtistId)
                    .HasColumnType("text")
                    .HasColumnName("artistId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.ToTable("genres");

                entity.Property(e => e.GenreId)
                    .HasColumnType("integer")
                    .HasColumnName("genreId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("name");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("url");
            });

            modelBuilder.Entity<Subgenre>(entity =>
            {
                entity.ToTable("subgenres");

                entity.Property(e => e.SubgenreId)
                    .HasColumnType("integer")
                    .HasColumnName("subgenreId");

                entity.Property(e => e.GenreId)
                    .HasColumnType("integer no")
                    .HasColumnName("genreId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("name");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("url");

                entity.HasOne(d => d.Genre)
                    .WithMany(p => p.Subgenres)
                    .HasForeignKey(d => d.GenreId);
            });

            modelBuilder.Entity<Track>(entity =>
            {
                entity.ToTable("tracks");

                entity.Property(e => e.TrackId)
                    .HasColumnType("text")
                    .HasColumnName("trackId");

                entity.Property(e => e.AlbumId)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("albumId");

                entity.Property(e => e.ArtistId)
                    .HasColumnType("text")
                    .HasColumnName("artistId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("name");

                entity.Property(e => e.SubgenreId)
                    .HasColumnType("integer")
                    .HasColumnName("subgenreId");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("url");

                entity.HasOne(d => d.Album)
                    .WithMany(p => p.Tracks)
                    .HasForeignKey(d => d.AlbumId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Artist)
                    .WithMany(p => p.Tracks)
                    .HasForeignKey(d => d.ArtistId);

                entity.HasOne(d => d.Subgenre)
                    .WithMany(p => p.Tracks)
                    .HasForeignKey(d => d.SubgenreId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
