// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Backoffice
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.Services.AddScoped(
                sp => new HttpClient {BaseAddress = new Uri("https://localhost:5001/api/music/")});
            await builder.Build().RunAsync();
        }
    }
}