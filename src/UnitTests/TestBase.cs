// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestBase.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTests
{
    using System;
    using AppServices.DataAccess;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using NUnit.Framework;

    public abstract class TestBase
    {
        protected IServiceProvider provider;

        protected IServiceCollection services;

        public const string DatabasePath = "Data Source=/home/lex/src/web-scraping/db/db.db";

        public virtual void RegisterDependencies()
        {
            this.services.AddLogging(builder => builder.AddSimpleConsole(options =>
            {
                options.IncludeScopes = true;
                options.SingleLine = true;
                options.TimestampFormat = "hh:mm:ss ";
            }));
            this.services.AddDbContext<WebScrapingContext>(builder =>
                {
                    builder.UseSqlite(DatabasePath);
                    builder.EnableSensitiveDataLogging();
                }
            );
        }

        [OneTimeSetUp]
        public void Setup()
        {
            this.services = new ServiceCollection();
            this.RegisterDependencies();
            this.provider = this.services.BuildServiceProvider();
        }
    }
}