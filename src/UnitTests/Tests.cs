// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SongFinder.cs" company="Raintech">
//   Raintech ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Raintech ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTests
{
    using System.Diagnostics;
    using System.Threading.Tasks;
    using AppServices;
    using AppServices.Impl;
    using Microsoft.Extensions.DependencyInjection;
    using NapsterExtractor;
    using NapsterExtractor.Impl;
    using NUnit.Framework;

    [TestFixture]
    public class Tests : TestBase
    {
        public override void RegisterDependencies()
        {
            base.RegisterDependencies();
            this.services.AddScoped<INapsterScraping, NapsterScraping>(
                _ => new NapsterScraping(NapsterScraping.Napster));
            this.services.AddAutoMapper(expression => expression.AddProfile<MapperProfile>());
            this.services.AddScoped<IExtractor, Extractor>();
            this.services.AddScoped<IMusicDataImporter, MusicDataImporter>();
            this.services.AddScoped<IMusicFinder, MusicFinder>();
        }

        [Order(1)]
        [Test(Description = "Inicialización de la base de datos.")]
        [Ignore("Esta prueba sólo se usará para inicializar la base de datos. (Comentar este atributo)")]
        public void ImportDataTest()
        {
            Assert.DoesNotThrowAsync(async () =>
                await this.provider.GetRequiredService<IMusicDataImporter>().CreateSourcesAsync());
        }

        [Order(2)]
        [TestCase("PO")]
        [Test(Description = "Busca géneros por palabra clave.")]
        public async Task GetGenresAsync(string genre)
        {
            var genres = await this.provider.GetRequiredService<IMusicFinder>().GetGenresAsync(genre);
            Assert.IsNotEmpty(genres);
            var g = genres[0];
            Debug.WriteLine("GetGenresAsync: Genre: {0} Subgenres: {1}", g.Name, g.SubgenresString);
        }

        [Order(3)]
        [TestCase("P")]
        [Test(Description = "Busca artistas por género.")]
        public async Task GetArtistsByGenderAsync(string genre)
        {
            var artists = await this.provider.GetRequiredService<IMusicFinder>().GetArtistsByGenderAsync(genre);
            Assert.IsNotEmpty(artists);
            var a = artists[0];
            Debug.WriteLine("GetArtistsByGenderAsync: Artist: {0} Genre: {1} ", a.Artist, a.Genre);
        }

        [Order(4)]
        [TestCase("BIL")]
        [Test(Description = "Busca albumes por artista.")]
        public async Task GetAlbumsByArtistAsync(string artist)
        {
            var albums = await this.provider.GetRequiredService<IMusicFinder>().GetAlbumsByArtistAsync(artist);
            Assert.IsNotEmpty(albums);
            var a = albums[0];
            Debug.WriteLine("GetAlbumsByArtistAsync: Album: {0} Artist: {1}", a.Album, a.Artist);
        }

        [Order(5)]
        [TestCase("COSMI")]
        [Test(Description = "Busca canciones por album.")]
        public async Task GetTracksByAlbumAsync(string album)
        {
            var tracks = await this.provider.GetRequiredService<IMusicFinder>().GetTracksByAlbumAsync(album);
            Assert.IsNotEmpty(tracks);
            var t = tracks[0];
            Debug.WriteLine("GetTracksByAlbumAsync: Track: {0} Album: {1}", t.Name, t.Album.Name);
        }

        [Order(6)]
        [TestCase("Another")]
        [Test(Description = "Busca artistas por canción.")]
        public async Task GetArtistByTrackAsync(string track)
        {
            var artists = await this.provider.GetRequiredService<IMusicFinder>().GetArtistByTrackAsync(track);
            Assert.IsNotEmpty(artists);
            var a = artists[0];
            Debug.WriteLine("GetArtistByTrackAsync: Artist: {0} Track: {1}", a.Artist, a.Track);
        }
    }
}