# Web Scraping

This project shows how to do web scraping and consume extracted data.

## EF Scaffold


dotnet ef dbcontext scaffold "Data Source=/home/lex/src/web-scraping/db/db.db" "Microsoft.EntityFrameworkCore.Sqlite" -f -c WebScrapingContext --context-dir DataAccess/Context -o /DataModel -v --no-build
